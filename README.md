INTRODUCTION
------------

> Zippy is a SCSS based Drupal 8 theme with modern design will help you to
> attract your site visitors. Used Latest Zurb Foundation version, customized
> theme options, and custom  block types will provide more control over the
> design.

* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/zippy


REQUIREMENTS
------------

* Zurb Foundation as a base theme


INSTALLATION
------------

* Install as you would normally install a contributed Drupal theme. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-themes
   for further information.

MAINTAINERS
-----------

Current maintainers:
 * Gaurav Kapoor - https://www.drupal.org/u/gauravkapoor
